import flask

app = flask.Flask(__name__)


@app.route('/', methods=['POST'])
def on_message():
  return flask.jsonify({
      'text': 'You said: %s' % flask.request.form['text'],
  })

